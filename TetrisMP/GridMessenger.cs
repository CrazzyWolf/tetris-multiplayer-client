﻿namespace TetrisMP
{
	class GridMessenger
	{
		public readonly object messengerLock = new object();
		public bool gridIsSet = false;
		//player 1
		public int[,] grid1;
		public int[] xBlocks1;
		public int[] yBlocks1;
		public int[] yHint1;
		public int shape1;
		//player 2
		public int[,] grid2;
		public int[] xBlocks2;
		public int[] yBlocks2;
		public int[] yHint2;
		public int shape2;
	}
}
