﻿using Microsoft.Xna.Framework;
using MonoGame.UI.Forms;
using System;

namespace TetrisMP
{
	public class MyControls : ControlManager
	{
		private int width, height, offscreen;
		private Vector2 menuLocation, pauseMenuLocation;
		private Form pauseMenu, menu;
		private Game1 game;
		private Button resumeButton, pauseNewGameButton, menuNewGameButton, quitButton, exitButton;

		public MyControls(Game1 game, int width, int height) : base(game)
		{
			this.game = game;
			this.width = width;
			this.height = height;
			menuLocation = new Vector2(0, 0);
			pauseMenuLocation = new Vector2(width / 4, height / 4);
			offscreen = -50000;
		}

		public override void InitializeComponent()
		{
			menu = new Form()
			{
				Title = "Menu",
				//FontName = "largeFont",
				IsMovable = false,
				Size = new Vector2(width, height),
			};

			pauseMenu = new Form()
			{
				Title = "Paused",
				IsMovable = true,
				Size = new Vector2(width / 2, height / 2),
				BackgroundColor = new Color(0, 0, 0),
			};

			Vector2 pauseButtonSize = new Vector2(pauseMenu.Size.X * 2 / 3, pauseMenu.Size.Y / 8);
			Vector2 menuButtonSize = new Vector2(menu.Size.X * 2 / 3, menu.Size.Y / 10);

			resumeButton = new Button()
			{
				Text = "Resume",
				Size = pauseButtonSize,
				Location = new Vector2
				(
					pauseMenu.Size.X / 2 - pauseButtonSize.X / 2,
					pauseMenu.Size.Y * 2 / 16
				),
				BackgroundColor = new Color(63, 63, 63),
			};

			pauseNewGameButton = new Button()
			{
				Text = "New game",
				Size = pauseButtonSize,
				Location = new Vector2
				(
					pauseMenu.Size.X / 2 - pauseButtonSize.X / 2,
					pauseMenu.Size.Y * 5 / 16
				),
				BackgroundColor = new Color(63, 63, 63),
			};

			quitButton = new Button()
			{
				Text = "Quit",
				Size = pauseButtonSize,
				Location = new Vector2
				(
					pauseMenu.Size.X / 2 - pauseButtonSize.X / 2,
					pauseMenu.Size.Y * 8 / 16
				),
				BackgroundColor = new Color(63, 63, 63),
			};

			menuNewGameButton = new Button()
			{
				Text = "New game",
				Size = menuButtonSize,
				Location = new Vector2
				(
					menu.Size.X / 2 - menuButtonSize.X / 2,
					menu.Size.Y * 2 / 16
				),
				BackgroundColor = new Color(63, 63, 63),
			};

			exitButton = new Button()
			{
				Text = "Exit",
				Size = menuButtonSize,
				Location = new Vector2
				(
					menu.Size.X / 2 - menuButtonSize.X / 2,
					menu.Size.Y * 5 / 16
				),
				BackgroundColor = new Color(63, 63, 63),
			};

			resumeButton.Clicked += Resume;
			pauseNewGameButton.Clicked += NewGame;
			quitButton.Clicked += Quit;

			menuNewGameButton.Clicked += NewGame;
			exitButton.Clicked += Exit;

			pauseMenu.Controls.Add(resumeButton);
			pauseMenu.Controls.Add(pauseNewGameButton);
			pauseMenu.Controls.Add(quitButton);

			menu.Controls.Add(menuNewGameButton);
			menu.Controls.Add(exitButton);

			//menu.Controls.ForEach(i => i.FontName = "largeFont");

			Controls.Add(menu);
			Controls.Add(pauseMenu);

			pauseMenuHide(true);
			menuHide(false);
		}

		public bool EscPressed()
		{
			if(isControlHidden(menu))
			{
				bool hidden = isControlHidden(pauseMenu);
				pauseMenuHide(!hidden);
				return hidden;
			}
			return false;
		}

		private void Resume(object sender, EventArgs e)
		{
			game.Continue();
			pauseMenuHide(true);
			menuHide(true);
		}

		private void menuHide(bool hide)
		{
			menu.Location = hide ? new Vector2(offscreen, 0) : menuLocation;
		}

		private void pauseMenuHide(bool hide)
		{
			pauseMenu.Location = hide ? new Vector2(offscreen, 0) : pauseMenuLocation;
		}

		private bool isControlHidden(Control control)
		{
			return control.Location.X == offscreen;
		}

		private void NewGame(object sender, EventArgs e)
		{
			game.RestartGame();
			Resume(sender, e);
		}

		private void Quit(object sender, EventArgs e)
		{
			menuHide(false);
			pauseMenuHide(true);
		}

		private void Exit(object sender, EventArgs e)
		{
			game.Exit();
		}

	}
}
