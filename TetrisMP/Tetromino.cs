﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace TetrisMP
{
    class Tetromino
    {
        public enum Shape { I, O, T, S, Z, J, L };

        private int numOfShapes = Enum.GetNames(typeof(Shape)).Length;
        private int[] xBlocks = new int[4];
        private int[] yBlocks = new int[4];
        private int rotation, x, y, defaultX, defaultY, lastShiftX, lastShiftY;
		private List<Shape> bag = new List<Shape>();
        private Shape shape;
        private readonly Texture2D[] colors;
        private Texture2D color;
        private readonly Random r;

        public Tetromino(int defaultX, int defaultY, Texture2D[] colors)
        {
            r = new Random();
            this.defaultX = defaultX;
            this.defaultY = defaultY;
            this.colors = colors;
            reset();
        }

        public void reset()
		{
			if(bag.Count == 0)
				bag.AddRange(Enum.GetValues(typeof(Shape)).Cast<Shape>());
			int random = r.Next(bag.Count);
			shape = bag[random];
			bag.RemoveAt(random);
			x = defaultX;
            y = defaultY;
            rotation = 0;
            setColor();
            update();
        }

        public void right()
        {
            for (int i = 0; i < xBlocks.Length; i++)
                xBlocks[i]++;
            x++;
        }

		public void left()
		{
			for(int i = 0; i < xBlocks.Length; i++)
				xBlocks[i]--;
			x--;
		}

		public void down()
        {
            for (int i = 0; i < xBlocks.Length; i++)
                yBlocks[i]++;
            y++;
        }

        public void up()
        {
            for (int i = 0; i < xBlocks.Length; i++)
                yBlocks[i]--;
            y--;
		}

		public void shift(int yShift, int xShift)
		{
			for(int i = 0; i < xBlocks.Length; i++)
			{
				xBlocks[i] += xShift;
				yBlocks[i] += yShift;
			}
			x += xShift;
			y += yShift;
			lastShiftX = xShift;
			lastShiftY = yShift;
		}

		public void rollbackShift()
		{
			for(int i = 0; i < xBlocks.Length; i++)
			{
				xBlocks[i] -= lastShiftX;
				yBlocks[i] -= lastShiftY;
			}
			x -= lastShiftX;
			y -= lastShiftY;
		}

		public void rotateLeft()
        {
            if (--rotation < 0)
                rotation = 3;
            update();
        }

        public void rotateRight()
        {
            if (++rotation > 3)
                rotation = 0;
            update();
        }

        public void nextShape()
        {
            shape = (Shape)(((int)shape + 1) % numOfShapes);
            setColor();
            update();
        }

        public void previousShape()
        {
            shape = (Shape)(((int)shape - 1) % numOfShapes);
            setColor();
            update();
        }

        public void setShape(Shape shape)
        {
            this.shape = shape;
            setColor();
            update();
        }

        private void setColor()
        {
			color = colors[(int)shape];
        }

        private void update()
        {
            switch (shape)
            {
                case Shape.I:
                    shapeI();
                    break;
                case Shape.O:
                    shapeO();
                    break;
                case Shape.T:
                    shapeT();
                    break;
                case Shape.S:
                    shapeS();
                    break;
                case Shape.Z:
                    shapeZ();
                    break;
                case Shape.J:
                    shapeJ();
                    break;
                case Shape.L:
                    shapeL();
                    break;
            }
        }

        private void shapeI()
        {
            switch (rotation)
			{
				/*
				case 0:
					xBlocks[0] = x;
					xBlocks[1] = x + 1;
					xBlocks[2] = x + 2;
					xBlocks[3] = x + 3;
					yBlocks[0] = yBlocks[1] = yBlocks[2] = yBlocks[3] = y + 1;
					break;
				case 1:
					xBlocks[0] = xBlocks[1] = xBlocks[2] = xBlocks[3] = x + 1;
					yBlocks[0] = y;
					yBlocks[1] = y + 1;
					yBlocks[2] = y + 2;
					yBlocks[3] = y + 3;
					break;
				case 2:
					xBlocks[0] = x - 1;
					xBlocks[1] = x;
					xBlocks[2] = x + 1;
					xBlocks[3] = x + 2;
					yBlocks[0] = yBlocks[1] = yBlocks[2] = yBlocks[3] = y + 1;
					break;
				case 3:
					xBlocks[0] = xBlocks[1] = xBlocks[2] = xBlocks[3] = x + 1;
					yBlocks[0] = y - 1;
					yBlocks[1] = y;
					yBlocks[2] = y + 1;
					yBlocks[3] = y + 2;
					break;
					*/
				case 0:
                    xBlocks[0] = x;
                    xBlocks[1] = x + 1;
                    xBlocks[2] = x + 2;
                    xBlocks[3] = x + 3;
                    yBlocks[0] = yBlocks[1] = yBlocks[2] = yBlocks[3] = y + 1;
                    break;
                case 1:
                    yBlocks[0] = y;
                    yBlocks[1] = y + 1;
                    yBlocks[2] = y + 2;
                    yBlocks[3] = y + 3;
                    xBlocks[0] = xBlocks[1] = xBlocks[2] = xBlocks[3] = x + 2;
                    break;
                case 2:
                    xBlocks[0] = x;
                    xBlocks[1] = x + 1;
                    xBlocks[2] = x + 2;
                    xBlocks[3] = x + 3;
                    yBlocks[0] = yBlocks[1] = yBlocks[2] = yBlocks[3] = y + 2;
                    break;
				case 3:
					yBlocks[0] = y;
					yBlocks[1] = y + 1;
					yBlocks[2] = y + 2;
					yBlocks[3] = y + 3;
					xBlocks[0] = xBlocks[1] = xBlocks[2] = xBlocks[3] = x + 1;
					break;
			}
		}
        private void shapeO()
        {
            xBlocks[0] = xBlocks[2] = x;
            xBlocks[1] = xBlocks[3] = x + 1;
            yBlocks[0] = yBlocks[1] = y;
            yBlocks[2] = yBlocks[3] = y + 1;
        }

        private void shapeT()
        {
            switch (rotation)
            {
                case 0:
                    xBlocks[0] = xBlocks[2] = x + 1;
                    xBlocks[1] = x;
                    xBlocks[3] = x + 2;
                    yBlocks[0] = y;
                    yBlocks[1] = yBlocks[2] = yBlocks[3] = y + 1;
                    break;
                case 1:
                    xBlocks[0] = xBlocks[2] = x + 1;
                    xBlocks[1] = x + 1;
                    xBlocks[3] = x + 2;
                    yBlocks[0] = y;
                    yBlocks[1] = y + 2;
                    yBlocks[2] = yBlocks[3] = y + 1;
                    break;
                case 2:
                    xBlocks[0] = xBlocks[2] = x + 1;
                    xBlocks[1] = x;
                    xBlocks[3] = x + 2;
                    yBlocks[0] = y + 2;
                    yBlocks[1] = yBlocks[2] = yBlocks[3] = y + 1;
                    break;
                case 3:
                    xBlocks[0] = xBlocks[2] = x + 1;
                    xBlocks[1] = x + 1;
                    xBlocks[3] = x;
                    yBlocks[0] = y;
                    yBlocks[1] = y + 2;
                    yBlocks[2] = yBlocks[3] = y + 1;
                    break;
            }
        }

        private void shapeS()
        {
            switch (rotation)
            {
                case 0:
                    xBlocks[0] = x;
                    xBlocks[1] = xBlocks[2] = x + 1;
                    xBlocks[3] = x + 2;
                    yBlocks[0] = yBlocks[1] = y + 1;
                    yBlocks[2] = yBlocks[3] = y;
                    break;
                case 1:
                    xBlocks[0] = xBlocks[1] = x + 1;
                    xBlocks[2] = xBlocks[3] = x + 2;
                    yBlocks[0] = y;
                    yBlocks[1] = yBlocks[2] = y + 1;
                    yBlocks[3] = y + 2;
                    break;
                case 2:
                    xBlocks[0] = x;
                    xBlocks[1] = xBlocks[2] = x + 1;
                    xBlocks[3] = x + 2;
                    yBlocks[0] = yBlocks[1] = y + 2;
                    yBlocks[2] = yBlocks[3] = y + 1;
                    break;
                case 3:
                    xBlocks[0] = xBlocks[1] = x;
                    xBlocks[2] = xBlocks[3] = x + 1;
                    yBlocks[0] = y;
                    yBlocks[1] = yBlocks[2] = y + 1;
                    yBlocks[3] = y + 2;
                    break;
            }
        }

        private void shapeZ()
        {
            switch (rotation)
            {
                case 0:
                    xBlocks[0] = x + 2;
                    xBlocks[1] = xBlocks[2] = x + 1;
                    xBlocks[3] = x;
                    yBlocks[0] = yBlocks[1] = y + 1;
                    yBlocks[2] = yBlocks[3] = y;
                    break;
                case 1:
                    xBlocks[0] = xBlocks[1] = x + 1;
                    xBlocks[2] = xBlocks[3] = x + 2;
                    yBlocks[0] = y + 2;
                    yBlocks[1] = yBlocks[2] = y + 1;
                    yBlocks[3] = y;
                    break;
                case 2:
                    xBlocks[0] = x + 2;
                    xBlocks[1] = xBlocks[2] = x + 1;
                    xBlocks[3] = x;
                    yBlocks[0] = yBlocks[1] = y + 2;
                    yBlocks[2] = yBlocks[3] = y + 1;
                    break;
                case 3:
                    xBlocks[0] = xBlocks[1] = x;
                    xBlocks[2] = xBlocks[3] = x + 1;
                    yBlocks[0] = y + 2;
                    yBlocks[1] = yBlocks[2] = y + 1;
                    yBlocks[3] = y;
                    break;
            }
        }

        private void shapeL()
        {
            switch (rotation)
            {
                case 0:
                    xBlocks[0] = x;
                    xBlocks[1] = x + 1;
                    xBlocks[2] = xBlocks[3] = x + 2;
                    yBlocks[0] = yBlocks[1] = yBlocks[2] = y + 1;
                    yBlocks[3] = y;
                    break;
                case 1:
                    xBlocks[0] = xBlocks[1] = xBlocks[2] = x + 1;
                    xBlocks[3] = x + 2;
                    yBlocks[0] = y;
                    yBlocks[1] = y + 1;
                    yBlocks[2] = yBlocks[3] = y + 2;
                    break;
                case 2:
                    xBlocks[0] = xBlocks[3] = x;
                    xBlocks[1] = x + 1;
                    xBlocks[2] = x + 2;
                    yBlocks[0] = yBlocks[1] = yBlocks[2] = y + 1;
                    yBlocks[3] = y + 2;
                    break;
                case 3:
                    xBlocks[0] = xBlocks[1] = xBlocks[2] = x + 1;
                    xBlocks[3] = x;
                    yBlocks[0] = yBlocks[3] = y;
                    yBlocks[1] = y + 1;
                    yBlocks[2] = y + 2;
                    break;
            }
        }

        private void shapeJ()
        {
            switch (rotation)
            {
                case 0:
                    xBlocks[0] = xBlocks[3] = x;
                    xBlocks[1] = x + 1;
                    xBlocks[2] = x + 2;
                    yBlocks[0] = yBlocks[1] = yBlocks[2] = y + 1;
                    yBlocks[3] = y;
                    break;
                case 1:
                    xBlocks[0] = xBlocks[1] = xBlocks[2] = x + 1;
                    xBlocks[3] = x + 2;
                    yBlocks[0] = yBlocks[3] = y;
                    yBlocks[1] = y + 1;
                    yBlocks[2] = y + 2;
                    break;
                case 2:
                    xBlocks[0] = x;
                    xBlocks[1] = x + 1;
                    xBlocks[2] = xBlocks[3] = x + 2;
                    yBlocks[0] = yBlocks[1] = yBlocks[2] = y + 1;
                    yBlocks[3] = y + 2;
                    break;
                case 3:
                    xBlocks[0] = xBlocks[1] = xBlocks[2] = x + 1;
                    xBlocks[3] = x;
                    yBlocks[0] = y;
                    yBlocks[1] = y + 1;
                    yBlocks[2] = yBlocks[3] = y + 2;
                    break;
            }
        }

		public Shape getShape()
		{
			return shape;
		}

        public int[] getX()
        {
            return xBlocks;
        }

        public int[] getY()
        {
            return yBlocks;
        }

        public Texture2D getColor()
        {
            return color;
        }

		public int getRotation()
		{
			return rotation;
		}
    }
}
