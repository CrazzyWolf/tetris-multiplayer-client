﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Windows.Forms;
using Keys = Microsoft.Xna.Framework.Input.Keys;

namespace TetrisMP
{
	public class Game1 : Game
	{
		private bool isGamePaused;
		private readonly object stateLock = new object();

		private readonly MyControls controls;

		private readonly GraphicsDeviceManager graphics;
		private SpriteBatch spriteBatch;

		private int blockSize;

		private KeyboardState previousState;

		private Tetromino tetromino;

		private Texture2D background, hint;
		private Texture2D[] tetrominoColors;
		private Texture2D[,] grid;

		private double timeSinceDown, previousGameTime;

		private int[] x, y, yHint;

		private bool skipPress, wasReset;

		public Game1()
		{
			IsMouseVisible = true;
			graphics = new GraphicsDeviceManager(this)
			{
				PreferredBackBufferHeight = Screen.PrimaryScreen.Bounds.Height - 100
			};
			graphics.PreferredBackBufferWidth = graphics.PreferredBackBufferHeight / 2;
			Content.RootDirectory = "Content";

			controls = new MyControls(
				this,
				graphics.PreferredBackBufferWidth,
				graphics.PreferredBackBufferHeight);
			Components.Add(controls);
		}

		protected override void Initialize()
		{
			base.Initialize();
		}

		protected override void LoadContent()
		{
			isGamePaused = true;

			timeSinceDown = 0.0;

			skipPress = false;

			background = Content.Load<Texture2D>("textury/tmavesediva");

			hint = Content.Load<Texture2D>("textury/transparent");

			tetrominoColors = new Texture2D[]
			{
				Content.Load<Texture2D>("textury/cyan"),
				Content.Load<Texture2D>("textury/zluta"),
				Content.Load<Texture2D>("textury/fialova"),
				Content.Load<Texture2D>("textury/modra"),
				Content.Load<Texture2D>("textury/oranzova"),
				Content.Load<Texture2D>("textury/zelena"),
				Content.Load<Texture2D>("textury/cervena"),
			};

			grid = new Texture2D[10, 20];

			blockSize = graphics.PreferredBackBufferWidth / grid.GetLength(0);

			tetromino = new Tetromino(grid.GetLength(0) / 2, 0, tetrominoColors);

			x = tetromino.getX();
			y = tetromino.getY();

			yHint = new int[x.Length];

			spriteBatch = new SpriteBatch(GraphicsDevice);

			//grid[1, 1] = Content.Load<Texture2D>("textury/unnamed");
			//Content.Load<Texture2D>("textury/svetleZelena"),
			//Content.Load<Texture2D>("textury/modroZelena"),
			//Content.Load<Texture2D>("textury/svetleModra"),
			//Content.Load<Texture2D>("textury/magneta"),
			//Content.Load<Texture2D>("textury/tmaveSediva"),
			//Content.Load<Texture2D>("textury/unnamed")
		}

		protected override void UnloadContent()
		{
			// TODO: Unload any non ContentManager content here
		}

		protected override void Update(GameTime gameTime)
		{
			KeyboardState state = Keyboard.GetState();

			if (state.IsKeyDown(Keys.Escape) && previousState.IsKeyUp(Keys.Escape))
			{
				isGamePaused = controls.EscPressed();
			}

			if (!isGamePaused)
			{
				UpdateHint();

				bool didMove = false;
				if (state.IsKeyDown(Keys.D) && previousState.IsKeyUp(Keys.D))
				{
					tetromino.right();
					if (NotValidPosition())
					{
						tetromino.left();
					}
					else
					{
						didMove = true;
					}
				}
				else if (state.IsKeyDown(Keys.A) && previousState.IsKeyUp(Keys.A))
				{
					tetromino.left();
					if (NotValidPosition())
					{
						tetromino.right();
					}
					else
					{
						didMove = true;
					}
				}

				if (state.IsKeyDown(Keys.Q) && previousState.IsKeyUp(Keys.Q))
				{
					tetromino.rotateLeft();
					if (NotValidPosition())
					{
						if (tetromino.getShape() == Tetromino.Shape.I)
						{
							if (!(didMove = KickILeft()))
							{
								tetromino.rotateRight();
							}
						}
						else
						{
							if (!(didMove = KickLeft()))
							{
								tetromino.rotateRight();
							}
						}
					}
					else
					{
						didMove = true;
					}
				}
				else if (state.IsKeyDown(Keys.E) && previousState.IsKeyUp(Keys.E))
				{
					tetromino.rotateRight();
					if (NotValidPosition())
					{
						if (tetromino.getShape() == Tetromino.Shape.I)
						{
							if (!(didMove = KickIRight()))
							{
								tetromino.rotateLeft();
							}
						}
						else
						{
							if (!(didMove = KickRight()))
							{
								tetromino.rotateLeft();
							}
						}
					}
					else
					{
						didMove = true;
					}
				}

				if (didMove && IsAtBottom())
				{
					timeSinceDown = gameTime.TotalGameTime.TotalMilliseconds;
				}

				if (!wasReset)
				{
					PressDown(state, gameTime);
				}
				else if (!previousState.IsKeyDown(Keys.S))
				{
					wasReset = false;
					PressDown(state, gameTime);
				}

				if (state.IsKeyDown(Keys.Space) && previousState.IsKeyUp(Keys.Space))
				{
					for (int i = 0; i < y.Length; i++)
					{
						y[i] = yHint[i];
					}

					ToBackground();
					tetromino.reset();
				}

				if (timeSinceDown + 1000.0 <= gameTime.TotalGameTime.TotalMilliseconds)
				{
					timeSinceDown = gameTime.TotalGameTime.TotalMilliseconds;
					MoveDown();
				}

				for (int i = 0; i < grid.GetLength(1); i++)
				{
					for (int j = 0; j < grid.GetLength(0); j++)
					{
						if (grid[j, i] == null)
						{
							goto skipRow;
						}
					}

					for (int j = 0; j < grid.GetLength(0); j++)
					{
						grid[j, i] = null;
					}

					for (int k = i; k > 0; k--)
					{
						for (int j = 0; j < grid.GetLength(0); j++)
						{
							grid[j, k] = grid[j, k - 1];
						}
					}

					skipRow: ;
				}
			}
			else
			{
				timeSinceDown += gameTime.TotalGameTime.TotalMilliseconds - previousGameTime;
			}

			base.Update(gameTime);
			previousGameTime = gameTime.TotalGameTime.TotalMilliseconds;
			previousState = state;
		}

		protected override void Draw(GameTime gameTime)
		{
			GraphicsDevice.Clear(Color.Black);
			spriteBatch.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend);
			GraphicsDevice.SamplerStates[0] = SamplerState.PointClamp;

			for (int i = 0; i < grid.GetLength(0); i++)
			{
				for (int j = 0; j < grid.GetLength(1); j++)
				{
					if (grid[i, j] != null)
					{
						spriteBatch.Draw(grid[i, j], new Rectangle(blockSize * i, blockSize * j, blockSize, blockSize),
							Color.White);
					}
					else
					{
						spriteBatch.Draw(background, new Rectangle(blockSize * i, blockSize * j, blockSize, blockSize),
							Color.White);
					}
				}
			}

			for (int i = 0; i < x.Length; i++)
			{
				spriteBatch.Draw(hint, new Rectangle(x[i] * blockSize, yHint[i] * blockSize, blockSize, blockSize),
					Color.White);
			}

			for (int i = 0; i < x.Length; i++)
			{
				spriteBatch.Draw(tetromino.getColor(),
					new Rectangle(x[i] * blockSize, y[i] * blockSize, blockSize, blockSize), Color.White);
			}

			spriteBatch.End();

			base.Draw(gameTime);
		}

		private bool GetGameState
		{
			get
			{
				lock (stateLock)
				{
					return isGamePaused;
				}
			}
			set
			{
				lock (stateLock)
				{
					isGamePaused = value;
				}
			}
		}

		public void Pause()
		{
			isGamePaused = true;
		}

		public void Continue()
		{
			isGamePaused = false;
		}

		public bool IsPaused()
		{
			return isGamePaused;
		}

		public void RestartGame()
		{
			for (int i = 0; i < grid.GetLength(0); i++)
			{
				for (int j = 0; j < grid.GetLength(1); j++)
				{
					grid[i, j] = null;
				}
			}

			tetromino.reset();
		}

		private void UpdateHint()
		{
			for (int i = 0; i < x.Length; i++)
			{
				yHint[i] = y[i];
			}

			while (!NotValidPosition(x, yHint))
			{
				for (int i = 0; i < y.Length; i++)
				{
					yHint[i]++;
				}
			}

			for (int i = 0; i < y.Length; i++)
			{
				yHint[i]--;
			}
		}

		private void PressDown(KeyboardState state, GameTime gameTime)
		{
			if (skipPress)
			{
				skipPress = false;
			}
			else if (state.IsKeyDown(Keys.S))
			{
				skipPress = true;
				MoveDown();
				timeSinceDown = gameTime.TotalGameTime.TotalMilliseconds;
			}
		}

		private void MoveDown()
		{
			tetromino.down();
			if (NotValidPosition())
			{
				tetromino.up();
				ToBackground();
				tetromino.reset();
				if (NotValidPosition())
				{
					EndGame();
				}
			}
		}

		private void EndGame()
		{
			//gameState = GameState.paused;
			RestartGame();
		}

		private bool IsAtBottom()
		{
			tetromino.down();
			bool bottom = NotValidPosition();
			tetromino.up();
			return bottom;
		}

		private void ToBackground()
		{
			for (int i = 0; i < x.Length; i++)
			{
				grid[x[i], y[i]] = tetromino.getColor();
			}

			wasReset = true;
		}

		private bool NotValidPosition()
		{
			return NotValidPosition(x, y);
		}

		private bool NotValidPosition(int[] x, int[] y)
		{
			for (int i = 0; i < x.Length; i++)
			{
				if (x[i] < 0 || y[i] < 0 || x[i] >= grid.GetLength(0) || y[i] >= grid.GetLength(1) ||
				    grid[x[i], y[i]] != null)
				{
					return true;
				}
			}

			return false;
		}

		private bool TryShift(int x, int y)
		{
			tetromino.shift(x, y);
			if (NotValidPosition())
			{
				tetromino.rollbackShift();
				return false;
			}
			else
			{
				return true;
			}
		}

		private bool KickRight()
		{
			switch (tetromino.getRotation())
			{
				case 0:
				{
					if (TryShift(-1, 0))
					{
						return true;
					}

					if (TryShift(-1, 1))
					{
						return true;
					}

					if (TryShift(0, -2))
					{
						return true;
					}

					if (TryShift(-1, -2))
					{
						return true;
					}

					break;
				}
				case 1:
				{
					if (TryShift(1, 0))
					{
						return true;
					}

					if (TryShift(1, -1))
					{
						return true;
					}

					if (TryShift(0, 2))
					{
						return true;
					}

					if (TryShift(1, 2))
					{
						return true;
					}

					break;
				}
				case 2:
				{
					if (TryShift(1, 0))
					{
						return true;
					}

					if (TryShift(1, 1))
					{
						return true;
					}

					if (TryShift(0, -2))
					{
						return true;
					}

					if (TryShift(1, -2))
					{
						return true;
					}

					break;
				}
				case 3:
				{
					if (TryShift(-1, 0))
					{
						return true;
					}

					if (TryShift(-1, -1))
					{
						return true;
					}

					if (TryShift(0, 2))
					{
						return true;
					}

					if (TryShift(-1, 2))
					{
						return true;
					}

					break;
				}
			}

			return false;
		}

		private bool KickLeft()
		{
			switch (tetromino.getRotation())
			{
				case 3:
				{
					if (TryShift(1, 0))
					{
						return true;
					}

					if (TryShift(1, -1))
					{
						return true;
					}

					if (TryShift(0, 2))
					{
						return true;
					}

					if (TryShift(1, 2))
					{
						return true;
					}

					break;
				}
				case 2:
				{
					if (TryShift(-1, 0))
					{
						return true;
					}

					if (TryShift(-1, 1))
					{
						return true;
					}

					if (TryShift(0, -2))
					{
						return true;
					}

					if (TryShift(-1, -2))
					{
						return true;
					}

					break;
				}
				case 1:
				{
					if (TryShift(-1, 0))
					{
						return true;
					}

					if (TryShift(-1, -1))
					{
						return true;
					}

					if (TryShift(0, 2))
					{
						return true;
					}

					if (TryShift(-1, 2))
					{
						return true;
					}

					break;
				}
				case 0:
				{
					if (TryShift(1, 0))
					{
						return true;
					}

					if (TryShift(1, 1))
					{
						return true;
					}

					if (TryShift(0, -2))
					{
						return true;
					}

					if (TryShift(1, -2))
					{
						return true;
					}

					break;
				}
			}

			return false;
		}

		private bool KickIRight()
		{
			switch (tetromino.getRotation())
			{
				case 0:
				{
					if (TryShift(-2, 0))
					{
						return true;
					}

					if (TryShift(1, 0))
					{
						return true;
					}

					if (TryShift(-2, -1))
					{
						return true;
					}

					if (TryShift(1, 2))
					{
						return true;
					}

					break;
				}
				case 1:
				{
					if (TryShift(-1, 0))
					{
						return true;
					}

					if (TryShift(2, 0))
					{
						return true;
					}

					if (TryShift(-1, 2))
					{
						return true;
					}

					if (TryShift(2, -1))
					{
						return true;
					}

					break;
				}
				case 2:
				{
					if (TryShift(2, 0))
					{
						return true;
					}

					if (TryShift(-1, 0))
					{
						return true;
					}

					if (TryShift(2, 1))
					{
						return true;
					}

					if (TryShift(-1, -2))
					{
						return true;
					}

					break;
				}
				case 3:
				{
					if (TryShift(1, 0))
					{
						return true;
					}

					if (TryShift(-2, 0))
					{
						return true;
					}

					if (TryShift(1, -2))
					{
						return true;
					}

					if (TryShift(-2, 1))
					{
						return true;
					}

					break;
				}
			}

			return false;
		}

		private bool KickILeft()
		{
			switch (tetromino.getRotation())
			{
				case 3:
				{
					if (TryShift(2, 0))
					{
						return true;
					}

					if (TryShift(-1, 0))
					{
						return true;
					}

					if (TryShift(2, 1))
					{
						return true;
					}

					if (TryShift(-1, -2))
					{
						return true;
					}

					break;
				}
				case 2:
				{
					if (TryShift(1, 0))
					{
						return true;
					}

					if (TryShift(-2, 0))
					{
						return true;
					}

					if (TryShift(1, -2))
					{
						return true;
					}

					if (TryShift(-2, 1))
					{
						return true;
					}

					break;
				}
				case 1:
				{
					if (TryShift(-2, 0))
					{
						return true;
					}

					if (TryShift(1, 0))
					{
						return true;
					}

					if (TryShift(-2, -1))
					{
						return true;
					}

					if (TryShift(1, 2))
					{
						return true;
					}

					break;
				}
				case 0:
				{
					if (TryShift(-1, 0))
					{
						return true;
					}

					if (TryShift(2, 0))
					{
						return true;
					}

					if (TryShift(-1, 2))
					{
						return true;
					}

					if (TryShift(2, -1))
					{
						return true;
					}

					break;
				}
			}

			return false;
		}
	}
}