﻿using System;
using System.Windows.Forms;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using MonoGame.UI.Forms;
using Keys = Microsoft.Xna.Framework.Input.Keys;

namespace TetrisMP
{
	public class GameMp : Game
	{
		private static int numberOfBlocks = 4;

		private readonly MyControlsMP controls;

		private readonly GraphicsDeviceManager graphics;
		private SpriteBatch spriteBatch;

		private int blockSize, shape1, shape2;

		private KeyboardState previousState, state;

		//private Tetromino tetromino;

		private Texture2D background, hint, black;
		private Texture2D[] tetrominoColors;
		private Texture2D[,] textureGrid;

		private GridMessenger messenger;

		private ServerConnection connection;

		private double lastRequestTime, lastRefreshTime, lastPingTime;

		private int[] xBlocks1, yBlocks1, yHint1, xBlocks2, yBlocks2, yHint2;

		//private char shape;

		private bool skipPress, wasReset, gridIsSet, isGamePaused;

		public GameMp()
		{
			IsMouseVisible = true;
			graphics = new GraphicsDeviceManager(this)
			{
				PreferredBackBufferHeight = Screen.PrimaryScreen.Bounds.Height * 3 / 4
			};
			graphics.PreferredBackBufferWidth = graphics.PreferredBackBufferHeight + graphics.PreferredBackBufferHeight / 20;
			Content.RootDirectory = "Content";

			controls = new MyControlsMP(this,
				graphics.PreferredBackBufferWidth,
				graphics.PreferredBackBufferHeight);
			Components.Add(controls);
		}

		protected override void Initialize()
		{
			base.Initialize();
		}

		protected override void LoadContent()
		{
			isGamePaused = true;

			skipPress = false;

			black = Content.Load<Texture2D>("textury/cerna");

			background = Content.Load<Texture2D>("textury/tmavesediva");

			hint = Content.Load<Texture2D>("textury/transparent");

			tetrominoColors = new Texture2D[]
			{
				Content.Load<Texture2D>("textury/cyan"),
				Content.Load<Texture2D>("textury/zluta"),
				Content.Load<Texture2D>("textury/fialova"),
				Content.Load<Texture2D>("textury/modra"),
				Content.Load<Texture2D>("textury/oranzova"),
				Content.Load<Texture2D>("textury/zelena"),
				Content.Load<Texture2D>("textury/cervena"),
			};

			textureGrid = new Texture2D[21, 20];

			blockSize = graphics.PreferredBackBufferWidth / textureGrid.GetLength(0);

			//tetromino = new Tetromino(textureGrid.GetLength(0) / 2, 0, tetrominoColors);

			xBlocks1 = new int[numberOfBlocks];
			yBlocks1 = new int[numberOfBlocks];
			yHint1 = new int[numberOfBlocks];

			xBlocks2 = new int[numberOfBlocks];
			yBlocks2 = new int[numberOfBlocks];
			yHint2 = new int[numberOfBlocks];

			spriteBatch = new SpriteBatch(GraphicsDevice);

			messenger = new GridMessenger();

			connection = new ServerConnection(messenger, controls);

			//Content.Load<Texture2D>("textury/svetleZelena"),
			//Content.Load<Texture2D>("textury/modroZelena"),
			//Content.Load<Texture2D>("textury/svetleModra"),
			//Content.Load<Texture2D>("textury/magneta"),
			//Content.Load<Texture2D>("textury/unnamed")
		}

		protected override void UnloadContent()
		{
			// TODO: Unload any non ContentManager content here
		}

		protected override void OnExiting(Object sender, EventArgs args)
		{
			base.OnExiting(sender, args);
			connection.EndConnection();
		}

		protected override void Update(GameTime gameTime)
		{
			state = Keyboard.GetState();

			if (controls.nickButtonFocused)
			{
				foreach(Keys key in state.GetPressedKeys())
				{
					if(previousState.IsKeyUp(key))
					{
						switch (key)
						{
							case Keys.Escape:
							case Keys.Enter:
								controls.nickButtonFocused = false;
								break;
							case Keys.Back:
								if(controls.nickButton.Text.Length > 0)
								{
									controls.nickButton.Text = controls.nickButton.Text.Remove(controls.nickButton.Text.Length - 1, 1);
								}
								break;
							case Keys.A:
							case Keys.B:
							case Keys.C:
							case Keys.D:
							case Keys.E:
							case Keys.F:
							case Keys.G:
							case Keys.H:
							case Keys.I:
							case Keys.J:
							case Keys.K:
							case Keys.L:
							case Keys.M:
							case Keys.N:
							case Keys.O:
							case Keys.P:
							case Keys.Q:
							case Keys.R:
							case Keys.S:
							case Keys.T:
							case Keys.U:
							case Keys.V:
							case Keys.W:
							case Keys.X:
							case Keys.Y:
							case Keys.Z:
								controls.nickButton.Text += key.ToString();
								break;
							case Keys.NumPad0:
								controls.nickButton.Text += "0";
								break;
							case Keys.NumPad1:
								controls.nickButton.Text += "1";
								break;
							case Keys.NumPad2:
								controls.nickButton.Text += "2";
								break;
							case Keys.NumPad3:
								controls.nickButton.Text += "3";
								break;
							case Keys.NumPad4:
								controls.nickButton.Text += "4";
								break;
							case Keys.NumPad5:
								controls.nickButton.Text += "5";
								break;
							case Keys.NumPad6:
								controls.nickButton.Text += "6";
								break;
							case Keys.NumPad7:
								controls.nickButton.Text += "7";
								break;
							case Keys.NumPad8:
								controls.nickButton.Text += "8";
								break;
							case Keys.NumPad9:
								controls.nickButton.Text += "9";
								break;
						}

						if (controls.nickButton.Text.Length >= MyControlsMP.nickMinLength &&
						    controls.nickButton.Text.Length <= MyControlsMP.nickMaxLength)
						{
							controls.nickButton.BackgroundColor = Color.Green;
							connection.nick = controls.nickButton.Text;
						}
						else
						{
							controls.nickButton.BackgroundColor = Color.DarkRed;
						}
					}
				}
			}

			if (controls.NeedsConnection())
			{
				if(controls.InGame() && gameTime.TotalGameTime.TotalMilliseconds > lastPingTime + 1000)
				{
					PingServer();
					lastPingTime = gameTime.TotalGameTime.TotalMilliseconds;
				}
				if(!connection.IsConnected())
				{
					controls.BackToPlayMenu();
					controls.ShowError(MyControlsMP.connectionLostMsg);
				}
			}

			if (controls.InLobby() && gameTime.TotalGameTime.TotalMilliseconds >= lastRefreshTime + 200)
			{
				lock (connection.lobbyLock)
				{
					for (int i = 0; i < controls.lobbyGameButtons.Count; i++)
					{
						int p1 = i * 2;
						int p2 = p1 + 1;
						controls.lobbyGameButtons[i].Text =
							connection.lobbyNicks[p1] + " VS " + connection.lobbyNicks[p2];

						if (connection.lobbyNicks[p1] != MyControlsMP.emptyNick &&
						    connection.lobbyNicks[p2] != MyControlsMP.emptyNick)
						{
							if (connection.lobbyNicks[p1].Equals(controls.nickButton.Text) ||
							    connection.lobbyNicks[p2].Equals(controls.nickButton.Text))
							{
								controls.lobbyGameButtons[i].Enabled = true;
								controls.lobbyGameButtons[i].BackgroundColor = Color.Green;
							}
							else
							{
								controls.lobbyGameButtons[i].Enabled = false;
								controls.lobbyGameButtons[i].BackgroundColor = controls.disabledButtonColor;
							}
						}
						else
						{
							controls.lobbyGameButtons[i].Enabled = true;
							controls.lobbyGameButtons[i].BackgroundColor = controls.buttonColor;
						}
					}
				}
				lastRefreshTime = gameTime.TotalGameTime.TotalMilliseconds;

				if (gameTime.TotalGameTime.TotalMilliseconds >= lastRequestTime + 1000)
				{
					RequestLobbyNames();
					lastRequestTime = gameTime.TotalGameTime.TotalMilliseconds;
				}
			}

			if(state.IsKeyDown(Keys.Escape) && previousState.IsKeyUp(Keys.Escape))
			{
				controls.EscPressed();
			}

			if(gridIsSet && !isGamePaused)
			{
				if(state.IsKeyDown(Keys.D) && previousState.IsKeyUp(Keys.D))
				{
					connection.SendMessage("d");
				}
				else if(state.IsKeyDown(Keys.A) && previousState.IsKeyUp(Keys.A))
				{
					connection.SendMessage("a");
				}

				if(state.IsKeyDown(Keys.Q) && previousState.IsKeyUp(Keys.Q))
				{
					connection.SendMessage("q");
				}
				else if(state.IsKeyDown(Keys.E) && previousState.IsKeyUp(Keys.E))
				{
					connection.SendMessage("e");
				}

				if(state.IsKeyDown(Keys.S) && previousState.IsKeyUp(Keys.S))
				{
					connection.SendMessage("s");
				}
				else if(state.IsKeyDown(Keys.Space) && previousState.IsKeyUp(Keys.Space))
				{
					connection.SendMessage("x");
				}
			}

			previousState = state;

			lock(messenger.messengerLock)
			{
				if(gridIsSet = messenger.gridIsSet)
				{
					//player 1
					for(int width = 0; width < messenger.grid1.GetLength(0); ++width)
					{
						for(int height = 0; height < messenger.grid1.GetLength(1); ++height)
						{
							if(messenger.grid1[width, height] == -1)
							{
								textureGrid[width, height] = null;
							}
							else
							{
								textureGrid[width, height] = tetrominoColors[messenger.grid1[width, height]];
							}
						}
					}

					for(int i = 0; i < xBlocks1.GetLength(0); i++)
					{
						xBlocks1[i] = messenger.xBlocks1[i];
					}

					for(int i = 0; i < yBlocks1.GetLength(0); i++)
					{
						yBlocks1[i] = messenger.yBlocks1[i];
					}

					for(int i = 0; i < yHint1.GetLength(0); i++)
					{
						yHint1[i] = messenger.yHint1[i];
					}

					shape1 = messenger.shape1;

					//player 2
					for(int width = 0; width < messenger.grid2.GetLength(0); ++width)
					{
						for(int height = 0; height < messenger.grid2.GetLength(1); ++height)
						{
							if(messenger.grid2[width, height] == -1)
							{
								textureGrid[width + messenger.grid2.GetLength(0) + 1, height] = null;
							}
							else
							{
								textureGrid[width + messenger.grid2.GetLength(0) + 1, height] = tetrominoColors[messenger.grid2[width, height]];
							}
						}
					}

					for(int i = 0; i < xBlocks2.GetLength(0); i++)
					{
						xBlocks2[i] = messenger.xBlocks2[i];
					}

					for(int i = 0; i < yBlocks2.GetLength(0); i++)
					{
						yBlocks2[i] = messenger.yBlocks2[i];
					}

					for(int i = 0; i < yHint2.GetLength(0); i++)
					{
						yHint2[i] = messenger.yHint2[i];
					}

					shape2 = messenger.shape2;
				}
			}

			base.Update(gameTime);
		}

		public void CleanGrid()
		{
			for (int i = 0; i < textureGrid.GetLength(0); i++)
			{
				for (int j = 0; j < textureGrid.GetLength(1); j++)
				{
					textureGrid[i, j] = null;
				}
			}

			xBlocks1[0] = -1;
			xBlocks2[0] = -1;
		}

		protected override void Draw(GameTime gameTime)
		{
			spriteBatch.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend);
			GraphicsDevice.Clear(Color.Black);
			GraphicsDevice.SamplerStates[0] = SamplerState.PointClamp;

			for (int i = 0; i < textureGrid.GetLength(0) / 2; i++)
			{
				for (int j = 0; j < textureGrid.GetLength(1); j++)
				{
					if (textureGrid[i, j] != null)
					{
						spriteBatch.Draw(textureGrid[i, j],
							new Rectangle(blockSize * i, blockSize * j, blockSize, blockSize),
							Color.White);
					}
					else
					{
						spriteBatch.Draw(background, new Rectangle(blockSize * i, blockSize * j, blockSize, blockSize),
							Color.White);
					}
				}
			}

			if (xBlocks1[0] != -1)
			{
				for(int i = 0; i < xBlocks1.Length; i++)
				{
					spriteBatch.Draw(hint, new Rectangle(xBlocks1[i] * blockSize, yHint1[i] * blockSize, blockSize, blockSize),
						Color.White);
				}

				for(int i = 0; i < xBlocks1.Length; i++)
				{
					spriteBatch.Draw(tetrominoColors[shape1],
						new Rectangle(xBlocks1[i] * blockSize, yBlocks1[i] * blockSize, blockSize, blockSize), Color.White);
				}
			}

			for(int j = 0; j < textureGrid.GetLength(1); j++)
			{
				spriteBatch.Draw(black,
					new Rectangle(blockSize * (textureGrid.GetLength(1) / 2), blockSize * j, blockSize, blockSize),
					Color.White);
			}

			int offset = textureGrid.GetLength(0) / 2 + 1;

			for(int i = offset; i < textureGrid.GetLength(0); i++)
			{
				for(int j = 0; j < textureGrid.GetLength(1); j++)
				{
					if(textureGrid[i, j] != null)
					{
						spriteBatch.Draw(textureGrid[i, j],
							new Rectangle(blockSize * i, blockSize * j, blockSize, blockSize),
							Color.White);
					}
					else
					{
						spriteBatch.Draw(background, new Rectangle(blockSize * i, blockSize * j, blockSize, blockSize),
							Color.White);
					}
				}
			}

			if (xBlocks2[0] != -1)
			{
				for(int i = 0; i < xBlocks2.Length; i++)
				{
					spriteBatch.Draw(hint, new Rectangle((offset + xBlocks2[i]) * blockSize, yHint2[i] * blockSize, blockSize, blockSize),
						Color.White);
				}

				for(int i = 0; i < xBlocks2.Length; i++)
				{
					spriteBatch.Draw(tetrominoColors[shape2],
						new Rectangle((offset + xBlocks2[i]) * blockSize, yBlocks2[i] * blockSize, blockSize, blockSize), Color.White);
				}
			}

			spriteBatch.End();

			base.Draw(gameTime);
		}

		public void PauseServer()
		{
			connection.SendMessage("v");
		}

		public void ContinueServer()
		{
			connection.SendMessage("c");
		}

		public void PauseGame()
		{
			isGamePaused = true;
		}

		public void ContinueGame()
		{
			isGamePaused = false;
		}

		public void RestartServer()
		{
			connection.SendMessage("y");
		}

		public void ConnectToGame(int slot)
		{
			connection.SendMessage("g" + slot);
		}

		public void DisconnectFromServer()
		{
			connection.EndConnection();
		}

		public bool ConnectToServer()
		{
			return connection.Start();
		}

		public void RequestLobbyNames()
		{
			connection.SendMessage("l");
		}

		public void PingServer()
		{
			connection.SendMessage("p");
		}

		public void ResetLobbyNicks()
		{
			lock(connection.lobbyLock)
			{
				for(int i = 0; i < connection.lobbyNicks.Length; i++)
				{
					connection.lobbyNicks[i] = MyControlsMP.emptyNick;
				}
			}
		}
	}
}