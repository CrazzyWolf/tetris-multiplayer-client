﻿using Microsoft.Xna.Framework;
using MonoGame.UI.Forms;
using System;
using System.Collections.Generic;
using Color = Microsoft.Xna.Framework.Color;

namespace TetrisMP
{
	public class MyControlsMP : ControlManager
	{
		public string winningPlayer;
		public bool nickButtonFocused;
		public Button nickButton;
		public List<Button> lobbyGameButtons;
		public readonly Color buttonColor, disabledButtonColor;
		public static readonly int nickMinLength = 3, nickMaxLength = 15;
		public static readonly string emptyNick = "Empty", couldNotConnectMsg = "Could not connect to server", connectionLostMsg = "Connection lost", 
			missingOpponentMsg = "Opponent is missing", opponentDisconnectMsg = "Opponent has\ndisconnected", opponentConnectMsg = "Opponent is\nconnected", winningPlayerMsg = " has won";

		private Form pauseMenu, menu, playMenu, lobbyMenu;
		private int width, height, offscreen;
		private Vector2 menuLocation, pauseMenuLocation, playMenuLocation, lobbyMenuLocation;
		private GameMp game;
		private Button backToPlayMenuButton, resumeButton, pauseNewGameButton, playButton, /*createLobbyButton,*/ joinLobbyButton, backToMenuButton, quitButton, exitButton;
		private string defaultNickText;
		private Label serverStateLabel, otherPlayerStateLabel;

		public MyControlsMP(GameMp game, int width, int height) : base(game)
		{
			this.game = game;
			this.width = width;
			this.height = height;
			menuLocation = new Vector2(0, 0);
			playMenuLocation = new Vector2(0, 0);
			lobbyMenuLocation = new Vector2(0, 0);
			pauseMenuLocation = new Vector2(width * 3.0f / 8.0f, height / 4.0f);
			buttonColor = new Color(63, 63, 63);
			disabledButtonColor = new Color(31, 31, 31);
			offscreen = -50000;
			nickButtonFocused = false;
			defaultNickText = "Enter Your Nickname";
		}

		public override void InitializeComponent()
		{
			menu = new Form()
			{
				Title = "Menu",
				IsMovable = false,
				Size = new Vector2(width, height),
				BackgroundColor = Color.Black
			};

			playMenu = new Form()
			{
				Title = "Play",
				IsMovable = false,
				Size = new Vector2(width, height),
				BackgroundColor = Color.Black
			};

			lobbyMenu = new Form()
			{
				Title = "Lobby",
				IsMovable = false,
				Size = new Vector2(width, height),
				BackgroundColor = Color.Black
			};

			pauseMenu = new Form()
			{
				Title = "Paused",
				IsMovable = true,
				Size = new Vector2(width / 4, height / 2),
				BackgroundColor = Color.Black
			};

			Vector2 pauseMenuButtonSize = new Vector2(pauseMenu.Size.X * 2 / 3, pauseMenu.Size.Y / 8);
			Vector2 menuButtonSize = new Vector2(menu.Size.X * 2 / 6, menu.Size.Y / 10);
			Vector2 playMenuButtonSize = new Vector2(playMenu.Size.X * 2 / 6, playMenu.Size.Y / 10);
			Vector2 lobbyMenuButtonSize = new Vector2(playMenu.Size.X * 2 / 6, playMenu.Size.Y / 20);

			lobbyGameButtons = new List<Button>();
			for(int i = 1; i <= 10; i++)
			{
				Button b = new Button()
				{
					Text = "",
					Size = lobbyMenuButtonSize,
					Location = new Vector2
					(
						playMenu.Size.X / 2 - lobbyMenuButtonSize.X / 2,
						playMenu.Size.Y / 16 + playMenu.Size.Y * i / 16
					),
					BackgroundColor = buttonColor,
				};
				lobbyGameButtons.Add(b);
			}

			backToPlayMenuButton = new Button()
			{
				Text = "Back",
				Size = playMenuButtonSize,
				Location = new Vector2
				(
					playMenu.Size.X / 2 - playMenuButtonSize.X / 2,
					playMenu.Size.Y * 12 / 16
				),
				BackgroundColor = buttonColor,
			};

			resumeButton = new Button()
			{
				Text = "Resume",
				Size = pauseMenuButtonSize,
				Location = new Vector2
				(
					pauseMenu.Size.X / 2 - pauseMenuButtonSize.X / 2,
					pauseMenu.Size.Y * 2 / 16
				),
				BackgroundColor = buttonColor,
			};

			pauseNewGameButton = new Button()
			{
				Text = "New game",
				Size = pauseMenuButtonSize,
				Location = new Vector2
				(
					pauseMenu.Size.X / 2 - pauseMenuButtonSize.X / 2,
					pauseMenu.Size.Y * 5 / 16
				),
				BackgroundColor = buttonColor,
			};

			quitButton = new Button()
			{
				Text = "Quit",
				Size = pauseMenuButtonSize,
				Location = new Vector2
				(
					pauseMenu.Size.X / 2 - pauseMenuButtonSize.X / 2,
					pauseMenu.Size.Y * 8 / 16
				),
				BackgroundColor = buttonColor,
			};

			otherPlayerStateLabel = new Label()
			{
				Text = "",
				Location = new Vector2
				(
					pauseMenu.Size.X / 2 - pauseMenuButtonSize.X / 2,
					pauseMenu.Size.Y * 11 / 16
				),
			};

			nickButton = new Button()
			{
				Text = defaultNickText,
				Size = menuButtonSize,
				Location = new Vector2
				(
					menu.Size.X / 2 - menuButtonSize.X / 2,
					menu.Size.Y * 2 / 16
				),
				BackgroundColor = buttonColor,
			};

			playButton = new Button()
			{
				Text = "Play",
				Size = menuButtonSize,
				Location = new Vector2
				(
					menu.Size.X / 2 - menuButtonSize.X / 2,
					menu.Size.Y * 5 / 16
				),
				BackgroundColor = buttonColor,
			};

			exitButton = new Button()
			{
				Text = "Exit",
				Size = menuButtonSize,
				Location = new Vector2
				(
					menu.Size.X / 2 - menuButtonSize.X / 2,
					menu.Size.Y * 8 / 16
				),
				BackgroundColor = buttonColor,
			};

			/*createLobbyButton = new Button()
			{
				Text = "Create Game",
				Size = playMenuButtonSize,
				Location = new Vector2
				(
					playMenu.Size.X / 2 - playMenuButtonSize.X / 2,
					playMenu.Size.Y * 2 / 16
				),
				BackgroundColor = buttonColor,
			};*/

			joinLobbyButton = new Button()
			{
				Text = "Join Game",
				Size = playMenuButtonSize,
				Location = new Vector2
				(
					playMenu.Size.X / 2 - playMenuButtonSize.X / 2,
					playMenu.Size.Y * 2 / 16
				),
				BackgroundColor = buttonColor,
			};

			backToMenuButton = new Button()
			{
				Text = "Back",
				Size = playMenuButtonSize,
				Location = new Vector2
				(
					playMenu.Size.X / 2 - playMenuButtonSize.X / 2,
					playMenu.Size.Y * 5 / 16
				),
				BackgroundColor = buttonColor,
			};

			serverStateLabel = new Label()
			{
				Text = "",
				Location = new Vector2
				(
					playMenu.Size.X / 2 - playMenuButtonSize.X / 2,
					playMenu.Size.Y * 11 / 16
				),
				TextColor = Color.DarkRed,
			};

			for (int i = 0; i < lobbyGameButtons.Count; i++)
			{
				lobbyGameButtons[i].Clicked += ConnectToGame;
			}
			backToPlayMenuButton.Clicked += BackToPlayMenu;

			resumeButton.Clicked += Resume;
			pauseNewGameButton.Clicked += NewGame;
			quitButton.Clicked += BackToPlayMenu;

			//createLobbyButton.Clicked += CreateLobby; //TODO ip serveru
			joinLobbyButton.Clicked += JoinLobby;
			backToMenuButton.Clicked += BackToMenu;

			nickButton.Clicked += Nick;
			playButton.Clicked += ToLobby;
			exitButton.Clicked += Exit;

			//playMenu.Controls.Add(createLobbyButton);
			playMenu.Controls.Add(joinLobbyButton);
			playMenu.Controls.Add(backToMenuButton);
			playMenu.Controls.Add(serverStateLabel);

			pauseMenu.Controls.Add(resumeButton);
			pauseMenu.Controls.Add(pauseNewGameButton);
			pauseMenu.Controls.Add(quitButton);
			pauseMenu.Controls.Add(otherPlayerStateLabel);

			menu.Controls.Add(nickButton);
			menu.Controls.Add(playButton);
			menu.Controls.Add(exitButton);

			lobbyMenu.Controls.AddRange(lobbyGameButtons);
			lobbyMenu.Controls.Add(backToPlayMenuButton);

			//menu.Controls.ForEach(i => i.FontName = "largeFont");

			Controls.Add(menu);
			Controls.Add(playMenu);
			Controls.Add(pauseMenu);
			Controls.Add(lobbyMenu);

			HideAllMenus();
			MenuHide(false);
		}

		private void HideAllMenus()
		{
			MenuHide(true);
			PlayMenuHide(true);
			PauseMenuHide(true);
			LobbyMenuHide(true);
			serverStateLabel.Text = "";
			otherPlayerStateLabel.Text = "";
		}

		private void JoinLobby(object sender, EventArgs e)
		{
			if (game.ConnectToServer())
			{
				game.ResetLobbyNicks();
				HideAllMenus();
				LobbyMenuHide(false);
			}
			else
			{
				ShowError(couldNotConnectMsg);
			}
		}

		public void ShowError(string error)
		{
			serverStateLabel.Text = error;
		}

		private void Nick(object sender, EventArgs e)
		{
			nickButton.BackgroundColor = buttonColor;
			nickButtonFocused = true;
			nickButton.Text = "";
		}

		private void ToLobby(object sender, EventArgs e)
		{
			if (nickButton.Text != defaultNickText && nickButton.Text.Length >= nickMinLength && nickButton.Text.Length <= nickMaxLength)
			{
				nickButtonFocused = false;
				HideAllMenus();
				PlayMenuHide(false);
				nickButton.BackgroundColor = Color.Green;
			}
			else
			{
				nickButton.Text = defaultNickText;
				nickButton.BackgroundColor = Color.DarkRed;
			}
		}

		public void EscPressed()
		{
			if(IsControlHidden(menu) && IsControlHidden(playMenu))
			{
				if(IsControlHidden(pauseMenu))
				{
					game.PauseServer();
				}
				else
				{
					game.ContinueServer();
				}
			}
		}

		public void Pause()
		{
			PauseMenuHide(false);
		}

		public void Continue()
		{
			otherPlayerStateLabel.Text = "";
			PauseMenuHide(true);
		}

		public void MissingOpponent()
		{
			PauseMenuHide(false);
			otherPlayerStateLabel.TextColor = Color.DarkRed;
			otherPlayerStateLabel.Text = missingOpponentMsg;
		}

		public void OpponentDisconnect()
		{
			PauseMenuHide(false);
			otherPlayerStateLabel.TextColor = Color.DarkRed;
			otherPlayerStateLabel.Text = opponentDisconnectMsg;
		}

		public void OpponentConnect()
		{
			otherPlayerStateLabel.TextColor = Color.Green;
			otherPlayerStateLabel.Text = opponentConnectMsg;
		}

		public void GameEnded()
		{
			otherPlayerStateLabel.TextColor = Color.Green;
			otherPlayerStateLabel.Text = winningPlayer + winningPlayerMsg;
		}

		public bool InLobby()
		{
			return !IsControlHidden(lobbyMenu);
		}

		public bool InGame()
		{
			return IsControlHidden(lobbyMenu) && IsControlHidden(playMenu) && IsControlHidden(menu);
		}

		public bool NeedsConnection()
		{
			return IsControlHidden(playMenu) && IsControlHidden(menu);
		}

		private void BackToMenu(object sender, EventArgs e)
		{
			HideAllMenus();
			MenuHide(false);
		}

		public void BackToPlayMenu()
		{
			game.DisconnectFromServer();
			game.PauseGame();
			HideAllMenus();
			PlayMenuHide(false);
		}

		private void BackToPlayMenu(object sender, EventArgs e)
		{
			BackToPlayMenu();
		}

		private void ConnectToGame(object sender, EventArgs e)
		{
			if (sender is Button button && button.Enabled)
			{
				game.CleanGrid();
				game.ConnectToGame(lobbyGameButtons.IndexOf(button));
				HideAllMenus();
				PauseMenuHide(false);
			}
		}

		private void Resume(object sender, EventArgs e)
		{
			game.ContinueServer();
			HideAllMenus();
		}

		private void MenuHide(bool hide)
		{
			menu.Location = hide ? new Vector2(offscreen, 0) : menuLocation;
		}

		private void PauseMenuHide(bool hide)
		{
			if (hide)
			{
				pauseMenu.Location = new Vector2(offscreen, 0);
				game.ContinueGame();
			}
			else
			{
				pauseMenu.Location = pauseMenuLocation;
				game.PauseGame();
			}
		}

		private void PlayMenuHide(bool hide)
		{
			playMenu.Location = hide ? new Vector2(offscreen, 0) : playMenuLocation;
		}

		private void LobbyMenuHide(bool hide)
		{
			lobbyMenu.Location = hide ? new Vector2(offscreen, 0) : lobbyMenuLocation;
		}

		private bool IsControlHidden(Control control)
		{
			return (int)control.Location.X == offscreen;
		}

		private void NewGame(object sender, EventArgs e)
		{
			game.RestartServer();
			Resume(sender, e);
		}

		private void Exit(object sender, EventArgs e)
		{
			game.Exit();
		}
	}
}
