﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Text;

namespace TetrisMP
{
	class ServerConnection
	{
		private enum MessageState
		{
			Ok, Error, /*Ack, Nack,*/ Welcome, Pause, Continue, MissingOpponent, OpponentDisconnect, OpponentConnect, GameEnded
		}

		public string nick;

		private int errorCounter;

		private bool exit;

		private bool welcomeAccepted = false;

		public readonly object lobbyLock = new object();

		public string[] lobbyNicks = new string[20];

		private readonly object messageLock = new object();

		private string msg = null;

		private readonly object sendMessageLock = new object();

		private GridMessenger messenger;

		private MyControlsMP controls;

		private NetworkStream networkStream;

		private Thread thread;

		private TcpClient client;

		public ServerConnection(GridMessenger messenger, MyControlsMP controls)
		{
			this.messenger = messenger;
			this.controls = controls;
		}

		public bool Start()
		{
			errorCounter = 0;
			exit = false;

			client = new TcpClient();
			try
			{
				client.Connect(/*IPAddress.Loopback*/IPAddress.Parse("10.147.19.206"), 10000);
			}
			catch(SocketException)
			{
				Console.WriteLine("Could not connect to server");
				return false;
			}

			Console.WriteLine("Connected to server");
			networkStream = client.GetStream();
			thread = new Thread(ReceiveData);
			thread.Start(client);
			return true;
		}

		public void SendMessage(string s)
		{
			while (networkStream == null || !welcomeAccepted && s[0] != 'm')
			{
				if (exit || client.Connected)
				{
					return;
				}
				Thread.Sleep(1);
			}

			byte[] buffer = Encoding.ASCII.GetBytes(s);

			/*if (s != "k")
			{
				Console.WriteLine(s);
			}*/

			try
			{
				lock(sendMessageLock)
				{
					networkStream.Write(buffer, 0, buffer.Length);

					/*if (s != "k" && s != "n")
					{
						string ack;

						while(true)
						{
							ack = GetAndDeleteMsg();
							if (ack != null)
							{
								break;
							}

							Thread.Sleep(1);

							if(exit)
							{
								return;
							}
						}

						if(ack == "n")
						{
							//lock(sendMessageLock)
							{
								networkStream.Write(buffer, 0, buffer.Length);
							}
						}
					}*/

					welcomeAccepted = true;
				}
			}
			catch(IOException)
			{
				Console.WriteLine("Error while sending message");
				//EndConnection();
			}
		}

		/*
		private string GetAndDeleteMsg()
		{
			string s = null;
			lock(messageLock)
			{
				if(msg != null)
				{
					s = string.Copy(msg);
					msg = null;
				}
			}
			return s;
		}
		*/

		public bool IsConnected()
		{
			return client.Connected;
		}

		/*private void SetMsg(string s)
		{
			while(true)
			{
				lock(messageLock)
				{
					if(msg == null)
					{
						msg = s;
						break;
					}
				}

				//Console.WriteLine("SetMsg");
				Thread.Sleep(1);

				if(exit)
				{
					return;
				}
			}
		}*/

		public void EndConnection()
		{
			if(thread != null && thread.IsAlive)
			{
				Console.WriteLine("Disconnecting from server");

				exit = true;

				networkStream.Close();

				lock(messenger.messengerLock)
				{
					messenger.gridIsSet = false;
				}

				client.Close();
				thread.Join();
			}
		}

		private void SendMessageAsynchronously(string s)
		{
			new Thread(SendMessageByNewThread).Start(s);
		}

		private void SendMessageByNewThread(Object s)
		{
			SendMessage(s.ToString());
		}

		private void ReceiveData(Object serverObject)
		{
			NetworkStream stream = ((TcpClient)serverObject).GetStream();
			byte[] receivedBytes = new byte[1024];

			try
			{
				int byteCount;
				while((byteCount = stream.Read(receivedBytes, 0, receivedBytes.Length)) > 0)
				{
					List<string> list = new List<string>
					{
						Encoding.ASCII.GetString(receivedBytes, 0, byteCount)
					};

					//Console.WriteLine(Encoding.ASCII.GetString(receivedBytes, 0, byteCount));

					while(list[0].Length > 0)
					{
						int length = 1;
						if (list[0][0] == 'g' && list[0].Length > 426)
						{
							length = 427;
						}
						else if(list[0][0] == 'l' && list[0].Length > 300)
						{
							length = 301;
						}
						else if(list[0][0] == 'e' && list[0].Length > MyControlsMP.nickMaxLength)
						{
							length = 1 + MyControlsMP.nickMaxLength;
						}
						list.Add(list[0].Substring(0, length));
						list[0] = list[0].Remove(0, length);
					}
					list.RemoveAt(0);

					foreach (var m in list)
					{
						MessageState s = HandleMassage(m);
						switch(s)
						{
							case MessageState.Welcome:
							{
								string n = nick;
								for (int i = nick.Length; i < MyControlsMP.nickMaxLength; i++)
								{
									n += "_";
								}
								SendMessageAsynchronously("m" + n);
								break;
							}
							case MessageState.Ok:
								break;
							case MessageState.Error:
								if (++errorCounter >= 3)
								{
									exit = true;
								}
								break;
							case MessageState.Pause:
								controls.Pause();
								break;
							case MessageState.Continue:
								controls.Continue();
								break;
							case MessageState.MissingOpponent:
								controls.MissingOpponent();
								break;
							case MessageState.OpponentDisconnect:
								controls.OpponentDisconnect();
								break;
							case MessageState.OpponentConnect:
								controls.OpponentConnect();
								break;
							case MessageState.GameEnded:
								controls.GameEnded();
								break;
							/*
							 case MessageState.Ack:
								SetMsg("k");
								break;
							case MessageState.Nack:
								SetMsg("n");
								break;
							*/
							default:
								throw new ArgumentOutOfRangeException();
						}
					}

					if(exit)
					{
						return;
					}
				}
			}
			catch(IOException)
			{
				Console.WriteLine("Disconnected from server");
			}
		}

		private MessageState HandleMassage(string m)
		{
			if (m[0] == 'l' && m.Length == 301)
			{
				lock (lobbyLock)
				{
					for (int i = 0; i < lobbyNicks.Length; i++)
					{
						lobbyNicks[i] = "";
						for (int j = 0; j < MyControlsMP.nickMaxLength; j++)
						{
							int bufferIndex = 1 + i * MyControlsMP.nickMaxLength + j;
							if (m[bufferIndex] == '_')
							{
								if (lobbyNicks[i].Length < 3)
								{
									lobbyNicks[i] = MyControlsMP.emptyNick;
								}
								break;
							}
							lobbyNicks[i] += m[bufferIndex];
						}
					}
				}

				return MessageState.Ok;
			}

			if(m[0] == 'g' && m.Length == 427)
			{
				const int numberOfBlocks = 4;
				const int numberOfShapes = 7;
				const int gridWidth = 10;
				const int gridHeight = 20;

				int[,] grid1 = new int[gridWidth, gridHeight];
				int[] xBlocks1 = new int[numberOfBlocks];
				int[] yBlocks1 = new int[numberOfBlocks];
				int[] yHint1 = new int[numberOfBlocks];
				int[,] grid2 = new int[gridWidth, gridHeight];
				int[] xBlocks2 = new int[numberOfBlocks];
				int[] yBlocks2 = new int[numberOfBlocks];
				int[] yHint2 = new int[numberOfBlocks];
				int shape1, shape2;

				int bufferIndex = 1;

				//player 1
				for(int width = 0; width < gridWidth; ++width)
				{
					for(int height = 0; height < gridHeight; ++height)
					{
						if (m[bufferIndex] == '_')
						{
							grid1[width, height] = -1;
						}
						else
						{
							grid1[width, height] = m[bufferIndex] - '0';
							if (grid1[width, height] < 0 || grid1[width, height] >= numberOfShapes)
							{
								Console.WriteLine("Délka: " + m.Length + " - Error: " + m);
								return MessageState.Error;
							}
						}

						bufferIndex++;
					}
				}

				for(int i = 0; i < numberOfBlocks; i++)
				{
					xBlocks1[i] = m[bufferIndex++] - '0';
					if(xBlocks1[i] < 0 || xBlocks1[i] >= gridWidth)
					{
						Console.WriteLine("Délka: " + m.Length + " - Error: " + m);
						return MessageState.Error;
					}
				}

				for(int i = 0; i < numberOfBlocks; i++)
				{
					yBlocks1[i] = m[bufferIndex++] - '0';
					if(yBlocks1[i] < 0 || yBlocks1[i] >= gridHeight)
					{
						Console.WriteLine("Délka: " + m.Length + " - Error: " + m);
						return MessageState.Error;
					}
				}

				for(int i = 0; i < numberOfBlocks; i++)
				{
					yHint1[i] = m[bufferIndex++] - '0';
					if(yHint1[i] < 0 || yHint1[i] >= gridHeight)
					{
						Console.WriteLine("Délka: " + m.Length + " - Error: " + m);
						return MessageState.Error;
					}
				}

				shape1 = m[bufferIndex++] - '0';
				if(shape1 < 0 || shape1 >= numberOfShapes)
				{
					Console.WriteLine("Délka: " + m.Length + " - Error: " + m);
					return MessageState.Error;
				}

				//player 2
				for(int width = 0; width < gridWidth; ++width)
				{
					for(int height = 0; height < gridHeight; ++height)
					{
						if(m[bufferIndex] == '_')
						{
							grid2[width, height] = -1;
						}
						else
						{
							grid2[width, height] = m[bufferIndex] - '0';
							if(grid2[width, height] < 0 || grid2[width, height] >= numberOfShapes)
							{
								Console.WriteLine("Délka: " + m.Length + " - Error: " + m);
								return MessageState.Error;
							}
						}

						bufferIndex++;
					}
				}

				for(int i = 0; i < numberOfBlocks; i++)
				{
					xBlocks2[i] = m[bufferIndex++] - '0';
					if(xBlocks2[i] < 0 || xBlocks2[i] >= gridWidth)
					{
						Console.WriteLine("Délka: " + m.Length + " - Error: " + m);
						return MessageState.Error;
					}
				}

				for(int i = 0; i < numberOfBlocks; i++)
				{
					yBlocks2[i] = m[bufferIndex++] - '0';
					if(yBlocks2[i] < 0 || yBlocks2[i] >= gridHeight)
					{
						Console.WriteLine("Délka: " + m.Length + " - Error: " + m);
						return MessageState.Error;
					}
				}

				for(int i = 0; i < numberOfBlocks; i++)
				{
					yHint2[i] = m[bufferIndex++] - '0';
					if(yHint2[i] < 0 || yHint2[i] >= gridHeight)
					{
						Console.WriteLine("Délka: " + m.Length + " - Error: " + m);
						return MessageState.Error;
					}
				}

				shape2 = m[bufferIndex] - '0';
				if(shape2 < 0 || shape2 >= numberOfShapes)
				{
					Console.WriteLine("Délka: " + m.Length + " - Error: " + m);
					return MessageState.Error;
				}

				lock (messenger.messengerLock)
				{
					messenger.grid1 = grid1;
					messenger.xBlocks1 = xBlocks1;
					messenger.yBlocks1 = yBlocks1;
					messenger.yHint1 = yHint1;
					messenger.shape1 = shape1;

					messenger.grid2 = grid2;
					messenger.xBlocks2 = xBlocks2;
					messenger.yBlocks2 = yBlocks2;
					messenger.yHint2 = yHint2;
					messenger.shape2 = shape2;

					messenger.gridIsSet = true;
				}

				return MessageState.Ok;
			}

			if (m[0] == 'e' && m.Length == 1 + MyControlsMP.nickMaxLength)
			{
				controls.winningPlayer = "";
				for(int j = 1; j < 1 + MyControlsMP.nickMaxLength; j++)
				{
					if(m[j] == '_')
					{
						break;
					}
					controls.winningPlayer += m[j];
				}
				return MessageState.GameEnded;
			}

			switch (m[0])
			{
				case 'c':
					return MessageState.Continue;
				case 'v':
					return MessageState.Pause;
				case 'w':
					return MessageState.Welcome;
				case 'n':
					return MessageState.MissingOpponent;
				case 'd':
					return MessageState.OpponentDisconnect;
				case 'o':
					return MessageState.OpponentConnect;
				default:
					Console.WriteLine("Délka: " + m.Length + " - Error: " + m);
					return MessageState.Error;
			}
		}
	}
}
